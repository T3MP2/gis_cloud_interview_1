<?php

/**
 * Class MonotoneCubicSpline
 */
class MonotoneCubicSpline {
    protected $data = array();
    protected $m = array();

    /**
     * MonotoneCubicSpline constructor.
     * @param array $input - array with input data ([x, y] pairs)
     */
    public function __construct(array $input)
    {
        $this->data = $input;

        $n = count($input);
        $delta = array();
        $m = array();
        $alpha = array();
        $beta = array();
        $dist = array();
        $tau = array();

        // calculate $m
        for ($i = 0; $i < $n - 1; $i++) {
            $delta[$i] = ($input[$i + 1][1] - $input[$i][1]) / ($input[$i + 1][0] - $input[$i][0]);
            if ($i > 0) {
                $m[$i] = ($delta[$i - 1] + $delta[$i]) / 2;
            }
        }
        $m[0] = $delta[0];
        $m[$n - 1] = $delta[$n - 2];

        for ($i = 0; $i < $n - 1; $i++) {
            // fix $m where delta == 0
            if ($delta[$i] == 0) {
                $m[$i] = $m[$i + 1] = 0;
            }

            $alpha[$i] = $m[$i] / $delta[$i];
            $beta[$i] = $m[$i + 1] / $delta[$i];
            $dist[$i] = $alpha[$i] ** 2 + $beta[$i] ** 2;
            $tau[$i] = 3 / sqrt($dist[$i]);

            // recalculate $m where dist > 9
            if ($dist[$i] > 9) {
                $m[$i] = $tau[$i] * $alpha[$i] * $delta[$i];
                $m[$i + 1] = $tau[$i] * $beta[$i] * $delta[$i];
            }
        }

        $this->m = $m;
    }

    /**
     * Get interpolated value of y based on value of $x
     * @param float $x - x value to interpolate
     * @return float - y value
     */
    public function interpolate(float $x): float {
        // find location of $x in known datapoints
        for ($i = count($this->data) - 2; $i >= 0; $i--) {
            if ($this->data[$i][0] <= $x) break;
        }

        $h = $this->data[$i +  1][0] - $this->data[$i][0];
        $t = ($x - $this->data[$i][0]) / $h;
        $t2 = $t ** 2;
        $t3 = $t ** 3;
        $h00 = 2 * $t3 - 3 * $t2 + 1;
        $h10 = $t3 - 2 * $t2 + $t;
        $h01 = -2 * $t3 + 3 * $t2;
        $h11 = $t3 - $t2;

        $y = $h00 * $this->data[$i][1] + $h10 * $h * $this->m[$i] + $h01 * $this->data[$i + 1][1] + $h11 * $h * $this->m[$i + 1];
        return $y;
    }
}
