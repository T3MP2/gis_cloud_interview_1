<?php

require_once 'MonotoneCubicSpline.php';

$x = $_GET['x'] ?? null;

$points = [[0.1, 5], [0.3, 10], [0.5, 30], [0.6, 60], [0.8, 70]];
$lower_bound = $points[0][0];
$upper_bound = $points[count($points) - 1][0];

$p = new MonotoneCubicSpline($points);

if (isset($x)) {
    if ($x < $lower_bound || $x > $upper_bound) {
        echo "X: $x isn't whithin original datapoint bounds [$lower_bound, $upper_bound]<br>\n";
    } else {
        echo $p->interpolate($x);
    }
} else {
    // + 0.001 added because of floating point error
    for ($i = $lower_bound; $i <= $upper_bound + 0.001; $i += 0.02) {
        $result = $p->interpolate($i);
        echo "$i: $result<br>\n";
    }
}

